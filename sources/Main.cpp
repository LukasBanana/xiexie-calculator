/*
 * Main file
 * 
 * This file is part of the "XieXie-Calculator" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "WinFrame.h"

#include <wx/app.h>


class XXCalcApp : public wxApp
{
    
    public:

        virtual bool OnInit()
        {
            if (!wxApp::OnInit())
                return false;
                
            /* Create main window frame */
            WinFrame* Frame = new WinFrame("Xi�Xi� Calculator", wxDefaultPosition, wxSize(600, 250));

            Frame->Show(true);

            return true;
        }

};


IMPLEMENT_APP(XXCalcApp)



// ================================================================================