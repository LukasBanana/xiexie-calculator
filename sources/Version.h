/*
 * Version header
 * 
 * This file is part of the "XieXie-Calculator" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XXC_VERSION_H__
#define __XXC_VERSION_H__


#include <string>


/* === Version macros === */

#define __XIEXIE_CALCULATOR__

#define XXC_VERSION_MAJOR       0
#define XXC_VERSION_MINOR       1
#define XXC_VERSION_REVISION    2
#define XXC_VERSION_STATUS      "Alpha"


/* === Functions === */

std::string xxGetVersion();


#endif



// ================================================================================