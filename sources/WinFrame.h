/*
 * Window frame header
 * 
 * This file is part of the "XieXie-Calculator" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#ifndef __XXC_WINFRAME_H__
#define __XXC_WINFRAME_H__


#include <wx/frame.h>
#include <wx/textctrl.h>
#include <wx/font.h>
#include <wx/statusbr.h>


class WinFrame : public wxFrame
{

    public:
        
        WinFrame(const wxString& Title, const wxPoint& Pos, const wxSize& Size);
        virtual ~WinFrame();

        /* === Functinos === */

        bool ExecExpr(const wxString& Expr);

    private:

        /* === Functinos === */

        void CreateFont();
        void CreateInputCtrl();
        void CreateOutputCtrl();
        void CreateStatBar();

        long GetStyle() const;

        void ShowOutput(const wxArrayString& Out, bool SmallView = false);
        void ShowInfo();

        void OnTextEnter(wxCommandEvent& Event);

        /* === Members === */

        wxFont* StdFont_;
        wxFont* ErrFont_;

        wxTextCtrl* InputCtrl_;
        wxTextCtrl* OutputCtrl_;

        wxStatusBar* StatusBar_;

};


#endif



// ================================================================================