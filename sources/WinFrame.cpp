/*
 * Window frame file
 * 
 * This file is part of the "XieXie-Calculator" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "WinFrame.h"
#include "Version.h"

#include <wx/utils.h>
#include <wx/msgdlg.h>


WinFrame::WinFrame(const wxString& Title, const wxPoint& Pos, const wxSize& Size) :
    wxFrame(
        nullptr, wxID_ANY, Title, Pos, Size, GetStyle()
    ),
    StdFont_    (nullptr),
    ErrFont_    (nullptr),
    InputCtrl_  (nullptr),
    OutputCtrl_ (nullptr),
    StatusBar_  (nullptr)
{
    #ifdef _WINDOWS
    SetIcon(wxICON(app_icon));
    #endif

    /* Create window layout */
    CreateStatBar();
    CreateFont();
    CreateInputCtrl();
    CreateOutputCtrl();

    Centre();

    ShowInfo();
}
WinFrame::~WinFrame()
{
}

bool WinFrame::ExecExpr(const wxString& Expr)
{
    if (Expr.empty())
    {
        ShowInfo();
        return false;
    }

    /* Setup execution command for XieXie-Compiler */
    wxString Cmd;

    Cmd = "xxc -se -imm -e \"" + Expr + "\"";

    /* Execute command */
    wxArrayString Out, Err;
    long Result = wxExecute(Cmd, Out, Err);

    /* Show output */
    if (Result == 0)
    {
        bool HasError = (Out.GetCount() > 0 && Out[0].Left(5) == "ERROR");
        ShowOutput(Out, HasError);
        return true;
    }

    return false;
}


/*
 * ======= Private: =======
 */

static const int TextFieldSize = 25;
static const int Border = 10;

void WinFrame::CreateFont()
{
    /* Create standard font */
    StdFont_ = new wxFont(
        TextFieldSize/2,
        wxFONTFAMILY_DEFAULT,
        wxFONTSTYLE_NORMAL,
        wxFONTWEIGHT_LIGHT,
        false,
        "courier new"
    );

    /* Create error font */
    ErrFont_ = new wxFont(
        TextFieldSize/3,
        wxFONTFAMILY_DEFAULT,
        wxFONTSTYLE_NORMAL,
        wxFONTWEIGHT_LIGHT,
        false,
        "courier new"
    );
}

void WinFrame::CreateInputCtrl()
{
    const wxSize ClientSize = GetClientSize();

    InputCtrl_ = new wxTextCtrl(
        this, wxID_ANY, "",
        wxPoint(Border, Border),
        wxSize(ClientSize.GetWidth() - Border*2, TextFieldSize),
        wxTE_PROCESS_ENTER
    );

    InputCtrl_->SetFont(*StdFont_);

    InputCtrl_->Connect(wxEVT_TEXT_ENTER, wxCommandEventHandler(WinFrame::OnTextEnter), 0, this);
}

void WinFrame::CreateOutputCtrl()
{
    const wxSize ClientSize = GetClientSize();
    const int PosY = Border*2 + TextFieldSize;

    OutputCtrl_ = new wxTextCtrl(
        this, wxID_ANY, "",
        wxPoint(Border, PosY),
        wxSize(
            ClientSize.GetWidth() - Border*2,
            ClientSize.GetHeight() - PosY - Border
        ),
        wxTE_READONLY | wxTE_MULTILINE
    );

    OutputCtrl_->SetFont(*StdFont_);
}

void WinFrame::CreateStatBar()
{
    StatusBar_ = CreateStatusBar();
    StatusBar_->PushStatusText("Version " + xxGetVersion(), 0);
}

long WinFrame::GetStyle() const
{
    return wxSYSTEM_MENU | wxCAPTION | wxCLIP_CHILDREN | wxMINIMIZE_BOX | wxCLOSE_BOX;
}

void WinFrame::ShowOutput(const wxArrayString& Out, bool SmallView)
{
    wxString Str;

    for (size_t i = 0; i < Out.GetCount(); ++i)
        Str += Out[i] + "\n";

    OutputCtrl_->SetValue(Str);
    OutputCtrl_->SetFont(SmallView ? *ErrFont_ : *StdFont_);
}

void WinFrame::ShowInfo()
{
    /* Startup text */
    wxArrayString Info;
    {
        Info.Add("Welcome to the Xi�Xi� Calculator");
        Info.Add("");
        Info.Add("Copyright (C) 2014  Lukas Hermanns");
        Info.Add("Licensed under the terms of the GNU GPL Version 3");
        Info.Add("");
        Info.Add("Enter arithmetic expressions in the above text field");
        Info.Add("");
        Info.Add("Supported operators:");
        Info.Add("  A + B       A plus B");
        Info.Add("  A - B       A minus B");
        Info.Add("  A * B       A multiplied by B");
        Info.Add("  A / B       A divided by B");
        Info.Add("  A % B       A modulo B");
        Info.Add("  X << S      X left-shifted by S");
        Info.Add("  X >> S      X right-shifted by S");
        Info.Add("");
        Info.Add("Standard constants:");
        Info.Add("  pi          Number pi ~ 3.14...");
        Info.Add("  e           Euler's number e ~ 2.71...");
        Info.Add("");
        Info.Add("Supported functions:");
        Info.Add("  sin(X)      Sine of X");
        Info.Add("  cos(X)      Cosine of X");
        Info.Add("  tan(X)      Tangent of X");
        Info.Add("  asin(X)     Arc sine of X");
        Info.Add("  acos(X)     Arc cosine of X");
        Info.Add("  atan(X)     Arc tangent of X");
        Info.Add("  sinh(X)     Hyperbolic sine of X");
        Info.Add("  cosh(X)     Hyperbolic cosine of X");
        Info.Add("  tanh(X)     Hyperbolic tangent of X");
        Info.Add("  pow(B, E)   B power of E");
        Info.Add("  exp(X)      Exponetial function of X (e^X)");
        Info.Add("  sqrt(X)     Square root of X");
        Info.Add("  log(X)      Logarithm of X to base 10");
        Info.Add("  logb(X, B)  Logarithm of X to base B");
        Info.Add("  ln(X)       Natural logarithm of X to base e");
        Info.Add("  abs(X)      Absolute value of X");
        Info.Add("  fac(N)      Faculty of integral number N");
        Info.Add("");
        Info.Add("Extended functions:");
        Info.Add("  circle(R)       Area of circle with radius R");
        Info.Add("  ring(R)         Circumference with radius R");
        Info.Add("  rect(X, Y)      Area of rectangle with size X, Y");
        Info.Add("  sphere(R)       Volume of sphere with radius R");
        Info.Add("  box(X, Y, Z)    Volume of box with size X, Y, Z");
    }
    ShowOutput(Info, true);
}

void WinFrame::OnTextEnter(wxCommandEvent& Event)
{
    const wxString ExprStr = Event.GetString();
    ExecExpr(ExprStr);
}



// ================================================================================