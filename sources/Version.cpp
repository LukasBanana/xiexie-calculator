/*
 * Version file
 * 
 * This file is part of the "XieXie-Calculator" (Copyright (c) 2014 by Lukas Hermanns)
 * See "LICENSE.txt" for license information.
 */

#include "Version.h"

#include <sstream>


std::string xxGetVersion()
{
    std::stringstream SStr;
    SStr << XXC_VERSION_MAJOR << "." << XXC_VERSION_MINOR;

    #if XXC_VERSION_REVISION != 0
    SStr << " Rev. " << XXC_VERSION_REVISION;
    #endif
    #ifdef XXC_VERSION_STATUS
    SStr << " " << XXC_VERSION_STATUS;
    #endif

    return SStr.str();
}



// ================================================================================